<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home'); 
Route::get('/register', 'AuthController@register'); 
Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});


Route::group([
    'prefix' => 'post',
    'name' => 'post.'
], function(){
    Route::get('/', 'PostController@index')->name('index');
    Route::get('/create', 'PostController@create')->name('create');
    Route::post('/', 'PostController@store')->name('store');
    Route::get('/{post}', 'PostController@show')->name('show');
    Route::get('/{post}edit', 'PostController@edit')->name('edit');
    Route::put('/{post}', 'PostController@update')->name('update');
    Route::delete('/{post}', 'PostController@destroy')->name('destroy');
}
);

Route::group([
    'prefix' => 'casts',
    'as' => 'casts.'
], function(){
    Route::get('/', 'castController@index')->name('index');
    Route::get('/create', 'castController@create')->name('create');
    Route::post('/', 'castController@store')->name('store');
    Route::get('/{cast}', 'castController@show')->name('show');
    Route::get('/{cast}edit', 'castController@edit')->name('edit');
    Route::put('/{cast}', 'castController@update')->name('update');
    Route::delete('/{cast}', 'castController@destroy')->name('destroy');
}
);
